<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_stock extends CI_Model {
	public function select_all() {
		$this->db->select('*');
		$this->db->from('tbl_barang');

		$data = $this->db->get();

		return $data->result();
	}

	public function select_by_id($id) {
		$sql = "SELECT * FROM tbl_item WHERE id = '{$id}'";

		$data = $this->db->query($sql);

		return $data->row();
	}

	public function select_by_id_stock($id) {
		$sql = "SELECT * FROM tbl_barang WHERE id = '{$id}'";

		$data = $this->db->query($sql);

		return $data->result();
	}

	public function insert($data) {
		$sql = "INSERT INTO tbl_barang_masuk VALUES('','" .$data['id_item'] ."','" .$data['jumlah'] ."'," .$data['id_category'] ."," .$data['tanggal_masuk'] .")";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function insert_batch($data) {
		$this->db->insert_batch('kota', $data);

		return $this->db->affected_rows();
	}

	public function update($data) {
		$sql = "UPDATE tbl_item SET kode_item='" .$data['kode_item'] ."', nama_item='" .$data['nama_item'] ."', category_id=" .$data['category_id'] ." WHERE id='" .$data['id'] ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function delete($id) {
		$sql = "DELETE FROM tbl_item WHERE id='" .$id ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function check_nama($nama) {
		$this->db->where('nama', $nama);
		$data = $this->db->get('kota');

		return $data->num_rows();
	}

	public function total_rows() {
		$data = $this->db->get('kota');

		return $data->num_rows();
	}

	// stock


	public function barang_masuk() {
		$this->db->select('*');
		$this->db->from('tbl_barang_masuk');

		$data = $this->db->get();

		return $data->result();
	}

		public function barang_keluar() {
		$this->db->select('*');
		$this->db->from('tbl_barang_keluar');

		$data = $this->db->get();

		return $data->result();
	}

		function get_item($category_id){
		$query = $this->db->get_where('tbl_item', array('category_id' => $category_id));
		return $query;
	}

		function barang_laku () {

		$data = $this->db->query("SELECT category.nama_category, SUM(jumlah) jumlah FROM tbl_barang_keluar JOIN category ON tbl_barang_keluar.id_category = category.id_category GROUP BY tbl_barang_keluar.id_category ORDER BY `jumlah` DESC LIMIT 3");

		return $data->result();

		}
}

/* End of file M_kota.php */
/* Location: ./application/models/M_kota.php */
