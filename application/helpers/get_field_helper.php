<?php
/*
Creat By Trifaldi 12 September 2019
*/
if(!function_exists('get_field')){

		function get_field($id,$namatabel,$namafield='') // function memanggil field yang berisikan 'id'

		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('ID',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}
		function get_category($id,$namatabel,$namafield='') // function berfunsi memanggil field id_category
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('id_category',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}
		function get_subject($id,$namatabel,$namafield='') // function berfungsi memanggil field id_subject
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('id_subject',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}
		function get_problem($id,$namatabel,$namafield='') // function berfungsi memanggil field id_problem
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('id_problem',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}
		function get_worker($id,$namatabel,$namafield='') // fnction ini berfungsi memanggil field worker_id di table c_worker_personal untuk mendapatkan id telegram personal
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('worker_id',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}
		function get_brandid($id,$namatabel,$namafield='') // fnction ini berfungsi memanggil field worker_id di table c_worker_personal untuk mendapatkan id telegram personal
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('id_brand',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}

		function get_user_group($id,$namatabel,$namafield='') // fnction ini berfungsi memanggil field worker_id di table c_worker_personal untuk mendapatkan id telegram personal
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('user_id',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}

		function get_video($id,$namatabel,$namafield='') // fnction ini berfungsi memanggil field worker_id di table c_worker_personal untuk mendapatkan id telegram personal
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('id_video',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}

		function get_maskapai($id,$namatabel,$namafield='') // fnction ini berfungsi memanggil field worker_id di table c_worker_personal untuk mendapatkan id telegram personal
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('id_maskapai',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}

		function get_agent($id,$namatabel,$namafield='') // fnction ini berfungsi memanggil field worker_id di table c_worker_personal untuk mendapatkan id telegram personal
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('id_agent',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}
		function get_retur_id($id,$namatabel,$namafield='') // fnction ini berfungsi memanggil field worker_id di table c_worker_personal untuk mendapatkan id telegram personal
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('retur_id',$id);
			$Q=$ci->db->get($namatabel)->row();
			return $Q->field;
		}

		function get_field_array($id,$namatabel,$namafield='') // fnction ini berfungsi memanggil field worker_id di table c_worker_personal untuk mendapatkan id telegram personal
		{
			$ci = &get_instance();
			$ci->db->select($namafield." as field");
			$ci->db->where('ID');
			$Q=$ci->db->get($namatabel)->row_array();
			return $Q->field;
		}


	}



	if(!function_exists('get_photo')){



		function get_photo($foto,$jk,$namatabel,$class='')

		{

			if($foto)
			{
				$path = "foto/".$foto;
			}
			elseif($jk == "L")
			{
				$path = "assets/images/default_".$namatabel.".png";
			}
			elseif($jk == "P")
			{
				$path = "assets/images/default_".$namatabel."_2.png";
			}
			else
			{
				$path = "assets/images/tanda_tanya.png";
			}
			return '<img class="'.$class.'" src="'.base_url().$path.'">';

		}



	}
