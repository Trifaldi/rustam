<?php
  $no = 1;
  foreach ($dataStock as $stock) {
    ?>
    <tr>
      <td><?php echo $no; ?></td>
      <td><?php echo $stock->tanggal_masuk; ?></td>
      <td><?php echo get_field($stock->id_item,'tbl_item','kode_item'); ?></td>
      <td><?php echo get_category($stock->id_category,'category','nama_category'); ?></td>
      <td><?php echo get_field($stock->id_item,'tbl_item','nama_item'); ?></td>
      <td><?php echo $stock->jumlah; ?></td>
      <td class="text-center" style="min-width:230px;">
         <!--  <button class="btn btn-warning update-dataItem" data-id="<?php echo $stock->id; ?>"><i class="glyphicon glyphicon-repeat"></i> Update</button> -->
          <!-- <button class="btn btn-danger konfirmasiHapus-item" data-id="<?php echo $stock->id; ?>" data-toggle="modal" data-target="#konfirmasiHapus"><i class="glyphicon glyphicon-remove-sign"></i> Delete</button> -->
          <button class="btn btn-info detail-dataStock" data-id="<?php echo $stock->id; ?>"><i class="glyphicon glyphicon-info-sign"></i> Detail</button>
      </td>
    </tr>
    <?php
    $no++;
  }
?>
