<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Tambah Barang Masuk</h3>

  <form id="form-tambah-barang" method="POST">
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-calendar"></i>
      </span>
      <input type="date" class="form-control" placeholder="Tanggal Masuk" name="tanggal_masuk" aria-describedby="sizing-addon2">
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-home"></i>
      </span>
      <select name="id_category" class="form-control" id="category">
        <option value="">Pilih Category Item</option>
        <?php
        foreach ($dataCategory as $category) {
          ?>
          <option value="<?php echo $category->id_category; ?>">
            <?php echo $category->nama_category; ?>
          </option>
          <?php
        }
        ?>
      </select>
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-home"></i>
      </span>
      <select name="id_item" class="form-control" id="item">
        <!-- <option value="">Pilih Item</option>
        <?php
        foreach ($dataItem as $category) {
          ?>
          <option value="<?php echo $category->id; ?>">
            <?php echo $category->nama_item; ?>
          </option>
          <?php
        }
        ?> -->
      </select>
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-cog"></i>
      </span>
      <input type="text" class="form-control" placeholder="Jumlah" name="jumlah" aria-describedby="sizing-addon2">
    </div>
    <div class="form-group">
      <div class="col-md-12">
          <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Tambah Data</button>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
$(function () {
    $(".select2").select2();
});
</script>
<script type="text/javascript">
    $(document).ready(function(){

      $('#category').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('barangmasuk/get_item');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].nama_item+'</option>';
                        }
                        $('#item').html(html);

                    }
                });
                return false;
            }); 
            
    });
  </script>
