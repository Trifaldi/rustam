<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Update Item</h3>

  <form id="form-update-item" method="POST">
    <input type="hidden" name="id" value="<?php echo $dataItem->id; ?>">
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Kode Item" name="kode_item" aria-describedby="sizing-addon2" value="<?php echo $dataItem->kode_item; ?>">
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Nama Item" name="nama_item" aria-describedby="sizing-addon2" value="<?php echo $dataItem->nama_item; ?>">
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-home"></i>
      </span>
      <select name="category_id" class="form-control">
        <?php foreach ($dataCategory as $category) {?>
        <option <?php if ($category->id_category == $dataItem->category_id) {echo "selected";} ?> value="<?php echo $category->id_category; ?>"><?php echo $category->nama_category; ?></option>
          <?php
        }
        ?>
      </select>
    </div>
    <div class="form-group">
      <div class="col-md-12">
          <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Update Data</button>
      </div>
    </div>
  </form>
</div>
