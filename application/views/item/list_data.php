<?php
  $no = 1;
  foreach ($dataItem as $item) {
    ?>
    <tr>
      <td><?php echo $no; ?></td>
      <td><?php echo $item->kode_item; ?></td>
      <td><?php echo get_category($item->category_id,'category','nama_category'); ?></td>
      <td><?php echo $item->nama_item; ?></td>
      <td class="text-center" style="min-width:230px;">
          <button class="btn btn-warning update-dataItem" data-id="<?php echo $item->id; ?>"><i class="glyphicon glyphicon-repeat"></i> Update</button>
          <button class="btn btn-danger konfirmasiHapus-item" data-id="<?php echo $item->id; ?>" data-toggle="modal" data-target="#konfirmasiHapus"><i class="glyphicon glyphicon-remove-sign"></i> Delete</button>
          <!-- <button class="btn btn-info detail-dataKota" data-id="<?php echo $category->id_category; ?>"><i class="glyphicon glyphicon-info-sign"></i> Detail</button> -->
      </td>
    </tr>
    <?php
    $no++;
  }
?>
