<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
  <div class="box-header">
    <!-- <div class="col-md-6">
        <button class="form-control btn btn-primary" data-toggle="modal" data-target="#tambah-item"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
    </div> -->
    <div class="col-md-3">
        <a href="<?php echo base_url('Kota/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a>
    </div>
    <!-- <div class="col-md-3">
        <button class="form-control btn btn-default" data-toggle="modal" data-target="#import-kota"><i class="glyphicon glyphicon glyphicon-floppy-open"></i> Import Data Excel</button>
    </div> -->
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="list-data" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Kode Item</th>
          <th>Category Item</th>
          <th>Nama Item</th>
          <th>Stock</th>
          <th style="text-align: center;">Aksi</th>
        </tr>
      </thead>
      <tbody id="data-stock">

      </tbody>
    </table>
  </div>
</div>

<?php echo $modal_tambah_item; ?>

<div id="tempat-modal"></div>

<?php show_my_confirm('konfirmasiHapus', 'hapus-dataItem', 'Hapus Data Ini?', 'Ya, Hapus Data Ini'); ?>
<?php
  $data['judul'] = 'Kota';
  $data['url'] = 'Kota/import';
  echo show_my_modal('modals/modal_import', 'import-kota', $data);
?>

<script type="text/javascript">
var id_item;
$(document).on("click", ".konfirmasiHapus-item", function() {
  id_item = $(this).attr("data-id");
})
$(document).on("click", ".hapus-dataItem", function() {
  var id = id_item;

  $.ajax({
    method: "POST",
    url: "<?php echo base_url('Item/delete'); ?>",
    data: "id=" +id
  })
  .done(function(data) {
    $('#konfirmasiHapus').modal('hide');
    tampilItem();
    $('.msg').html(data);
    effect_msg();
  })
})
  $(document).on("click", ".update-dataItem", function() {
    var id = $(this).attr("data-id");

    $.ajax({
      method: "POST",
      url: "<?php echo base_url('Item/update'); ?>",
      data: "id=" +id
    })
    .done(function(data) {
      $('#tempat-modal').html(data);
      $('#update-item').modal('show');
    })
  })

$('#form-tambah-item').submit(function(e) {
  var data = $(this).serialize();

  $.ajax({
    method: 'POST',
    url: '<?php echo base_url('Item/prosesTambah'); ?>',
    data: data
  })
  .done(function(data) {
    var out = jQuery.parseJSON(data);

    tampilItem();
    if (out.status == 'form') {
      $('.form-msg').html(out.msg);
      effect_msg_form();
    } else {
      document.getElementById("form-tambah-item").reset();
      $('#tambah-item').modal('hide');
      $('.msg').html(out.msg);
      effect_msg();
    }
  })

  e.preventDefault();
});

$(document).on('submit', '#form-update-item', function(e){
  var data = $(this).serialize();

  $.ajax({
    method: 'POST',
    url: '<?php echo base_url('Item/prosesUpdate'); ?>',
    data: data
  })
  .done(function(data) {
    var out = jQuery.parseJSON(data);

    tampilItem();
    if (out.status == 'form') {
      $('.form-msg').html(out.msg);
      effect_msg_form();
    } else {
      document.getElementById("form-update-item").reset();
      $('#update-item').modal('hide');
      $('.msg').html(out.msg);
      effect_msg();
    }
  })

  e.preventDefault();
});

  $(document).on("click", ".detail-dataStock", function() {
    var id = $(this).attr("data-id");

    $.ajax({
      method: "POST",
      url: "<?php echo base_url('Stock/detail'); ?>",
      data: "id=" +id
    })
    .done(function(data) {
      $('#tempat-modal').html(data);
      $('#tabel-detail').dataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      $('#detail-stock').modal('show');
    })
  })

$('#tambah-item').on('hidden.bs.modal', function () {
  $('.form-msg').html('');
})

$('#update-item').on('hidden.bs.modal', function () {
  $('.form-msg').html('');
})
</script>
