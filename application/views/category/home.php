<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
  <div class="box-header">
    <div class="col-md-6">
        <button class="form-control btn btn-primary" data-toggle="modal" data-target="#tambah-category"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
    </div>
    <div class="col-md-3">
        <a href="<?php echo base_url('Kota/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a>
    </div>
    <div class="col-md-3">
        <button class="form-control btn btn-default" data-toggle="modal" data-target="#import-kota"><i class="glyphicon glyphicon glyphicon-floppy-open"></i> Import Data Excel</button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="list-data" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Nama Category</th>
          <th style="text-align: center;">Aksi</th>
        </tr>
      </thead>
      <tbody id="data-category">

      </tbody>
    </table>
  </div>
</div>

<?php echo $modal_tambah_category; ?>

<div id="tempat-modal"></div>

<?php show_my_confirm('konfirmasiHapus', 'hapus-dataCategory', 'Hapus Data Ini?', 'Ya, Hapus Data Ini'); ?>
<?php
  $data['judul'] = 'Kota';
  $data['url'] = 'Kota/import';
  echo show_my_modal('modals/modal_import', 'import-kota', $data);
?>

<script type="text/javascript">
var id_category;
$(document).on("click", ".konfirmasiHapus-category", function() {
  id_category = $(this).attr("data-id");
})
$(document).on("click", ".hapus-dataCategory", function() {
  var id = id_category;

  $.ajax({
    method: "POST",
    url: "<?php echo base_url('Category/delete'); ?>",
    data: "id=" +id
  })
  .done(function(data) {
    $('#konfirmasiHapus').modal('hide');
    tampilCategory();
    $('.msg').html(data);
    effect_msg();
  })
})
  $(document).on("click", ".update-dataCategory", function() {
    var id = $(this).attr("data-id");

    $.ajax({
      method: "POST",
      url: "<?php echo base_url('Category/update'); ?>",
      data: "id=" +id
    })
    .done(function(data) {
      $('#tempat-modal').html(data);
      $('#update-category').modal('show');
    })
  })

$('#form-tambah-category').submit(function(e) {
  var data = $(this).serialize();

  $.ajax({
    method: 'POST',
    url: '<?php echo base_url('Category/prosesTambah'); ?>',
    data: data
  })
  .done(function(data) {
    var out = jQuery.parseJSON(data);

    tampilCategory();
    if (out.status == 'form') {
      $('.form-msg').html(out.msg);
      effect_msg_form();
    } else {
      document.getElementById("form-tambah-category").reset();
      $('#tambah-category').modal('hide');
      $('.msg').html(out.msg);
      effect_msg();
    }
  })

  e.preventDefault();
});

$(document).on('submit', '#form-update-category', function(e){
  var data = $(this).serialize();

  $.ajax({
    method: 'POST',
    url: '<?php echo base_url('Category/prosesUpdate'); ?>',
    data: data
  })
  .done(function(data) {
    var out = jQuery.parseJSON(data);

    tampilCategory();
    if (out.status == 'form') {
      $('.form-msg').html(out.msg);
      effect_msg_form();
    } else {
      document.getElementById("form-update-category").reset();
      $('#update-category').modal('hide');
      $('.msg').html(out.msg);
      effect_msg();
    }
  })

  e.preventDefault();
});

$('#tambah-category').on('hidden.bs.modal', function () {
  $('.form-msg').html('');
})

$('#update-category').on('hidden.bs.modal', function () {
  $('.form-msg').html('');
})
</script>
