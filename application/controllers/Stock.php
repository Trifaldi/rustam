<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends AUTH_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->model('M_item');
		$this->load->model('M_category');
		$this->load->model('M_stock');
	}

	public function index () {

		$data['userdata'] 	= $this->userdata;
		$data['dataItem'] 	= $this->M_item->select_all();
		$data['dataCategory'] 	= $this->M_category->select_all();
		$data['dataStock'] 	= $this->M_stock->select_all();

		$data['page'] 		= "stock";
		$data['judul'] 		= "List item";
		$data['deskripsi'] 	= "Manage  Data Item";

		$data['modal_tambah_item'] = show_my_modal('modals/modal_tambah_item', 'tambah-item', $data);

		$this->template->views('stock/home', $data);
	}

	public function tampil() {
		$data['dataStock'] = $this->M_stock->select_all();
		$this->load->view('stock/list_data', $data);
	}

	// public function prosesTambah() {
	// 	$this->form_validation->set_rules('kode_item', 'Kode Item', 'trim|required');
	// 	$this->form_validation->set_rules('category_id', 'Category', 'trim|required');

	// 	$data 	= $this->input->post();
	// 	if ($this->form_validation->run() == TRUE) {
	// 		$result = $this->M_item->insert($data);

	// 		if ($result > 0) {
	// 			$out['status'] = '';
	// 			$out['msg'] = show_succ_msg('Item Berhasil ditambahkan', '20px');
	// 		} else {
	// 			$out['status'] = '';
	// 			$out['msg'] = show_err_msg('Item Gagal ditambahkan', '20px');
	// 		}
	// 	} else {
	// 		$out['status'] = 'form';
	// 		$out['msg'] = show_err_msg(validation_errors());
	// 	}

	// 	echo json_encode($out);
	// }

	// 	public function update() {
	// 	$data['userdata'] 	= $this->userdata;

	// 	$id 				= trim($_POST['id']);
	// 	$data['dataItem'] 	= $this->M_item->select_by_id($id);
	// 	$data['dataCategory'] 	= $this->M_category->select_all();

	// 	echo show_my_modal('item/modal_update_item', 'update-item', $data);
	// }

	// public function prosesUpdate() {
	// 	$this->form_validation->set_rules('kode_item', 'Kode Item', 'trim|required');

	// 	$data 	= $this->input->post();
	// 	if ($this->form_validation->run() == TRUE) {
	// 		$result = $this->M_item->update($data);

	// 		if ($result > 0) {
	// 			$out['status'] = '';
	// 			$out['msg'] = show_succ_msg('Item Berhasil diupdate', '20px');
	// 		} else {
	// 			$out['status'] = '';
	// 			$out['msg'] = show_succ_msg('Item Gagal diupdate', '20px');
	// 		}
	// 	} else {
	// 		$out['status'] = 'form';
	// 		$out['msg'] = show_err_msg(validation_errors());
	// 	}

	// 	echo json_encode($out);
	// }

	// public function delete() {
	// 	$id = $_POST['id'];
	// 	$result = $this->M_item->delete($id);

	// 	if ($result > 0) {
	// 		echo show_succ_msg('Item Berhasil dihapus', '20px');
	// 	} else {
	// 		echo show_err_msg('Item Gagal dihapus', '20px');
	// 	}
	// }

	public function detail() {
		$data['userdata'] 	= $this->userdata;

		$id 				= trim($_POST['id']);
		// $data['kota'] = $this->M_kota->select_by_id($id);
		// $data['jumlahKota'] = $this->M_kota->total_rows();
		$data['dataStock'] = $this->M_stock->select_by_id_stock($id);

		echo show_my_modal('stock/modal_detail_stock', 'detail-stock', $data, 'lg');
	}


}