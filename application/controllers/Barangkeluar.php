<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangkeluar extends AUTH_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->model('M_item');
		$this->load->model('M_category');
		$this->load->model('M_stock');
	}

	public function index () {

		$data['userdata'] 	= $this->userdata;
		$data['dataItem'] 	= $this->M_item->select_all();
		$data['dataCategory'] 	= $this->M_category->select_all();
		$data['dataStock'] 	= $this->M_stock->barang_masuk();

		$data['page'] 		= "barangkeluar";
		$data['judul'] 		= "List Barang Keluar";
		$data['deskripsi'] 	= "Manage  Data Item";

		$data['modal_tambah_barang'] = show_my_modal('barangkeluar/modal_tambah_barang', 'tambah-barang', $data);

		$this->template->views('barangkeluar/home', $data);
	}

	public function tampil() {
		$data['dataStock'] = $this->M_stock->barang_keluar();
		$this->load->view('barangkeluar/list_data', $data);
	}
	function get_item(){
		$category_id = $this->input->post('id',TRUE);
		$data = $this->M_stock->get_item($category_id)->result();
		echo json_encode($data);
	}

	public function prosesTambah() {
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('tanggal_keluar', 'Tanggal Keluar', 'trim|required');

		$id_item 	= $this->input->post('id_item');
		$jumlah 	= $this->input->post('jumlah');
		$id_category 	= $this->input->post('id_category');
		$tanggal_keluar 	= $this->input->post('tanggal_keluar');

		if ($this->form_validation->run() == TRUE) {
			$array = array('category_item' => $id_item, 'category_id' => $id_category );
		$this->db->where($array);
			$q = $this->db->get('tbl_barang');

			if ( $q->num_rows() > 0 )
			{
					$result = array(
			        'id_item' => $id_item,
			        'jumlah' => $jumlah,
			        'id_category' => $id_category,
			        'tanggal_keluar' => $tanggal_keluar
					);

					$this->db->insert('tbl_barang_keluar', $result);

					$array = array('category_item' => $id_item, 'category_id' => $id_category );
					$this->db->where($array);
					$select = $this->db->get('tbl_barang')->row();

					//echo $select;

					$stock = array(
			        'category_item' => $id_item,
			        'jumlah_barang' => $select->jumlah_barang - $jumlah,
			        'category_id' => $id_category
					);

				$array = array('category_item' => $id_item, 'category_id' => $id_category );
				$this->db->where($array);
				$this->db->update('tbl_barang',$stock);

						if ($result > 0) {
							$out['status'] = '';
							$out['msg'] = show_succ_msg('Item Berhasil ditambahkan', '20px');
						} else {
							$out['status'] = '';
							$out['msg'] = show_err_msg('Item Gagal ditambahkan', '20px');
						}
			} else {
					$result = array(
			        'id_item' => $id_item,
			        'jumlah' => $jumlah,
			        'id_category' => $id_category,
			        'tanggal_keluar' => $tanggal_keluar
					);

					$this->db->insert('tbl_barang_masuk', $result);

					$stock = array(
			        'category_item' => $id_item,
			        'jumlah_barang' => $jumlah,
			        'category_id' => $id_category
					);

					$this->db->insert('tbl_barang', $stock);



						if ($result > 0) {
							$out['status'] = '';
							$out['msg'] = show_succ_msg('Item Berhasil ditambahkan', '20px');
						} else {
							$out['status'] = '';
							$out['msg'] = show_err_msg('Item Gagal ditambahkan', '20px');
						}
					}

		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

		public function update() {
		$data['userdata'] 	= $this->userdata;

		$id 				= trim($_POST['id']);
		$data['dataItem'] 	= $this->M_item->select_by_id($id);
		$data['dataCategory'] 	= $this->M_category->select_all();

		echo show_my_modal('item/modal_update_item', 'update-item', $data);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('kode_item', 'Kode Item', 'trim|required');

		$data 	= $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$result = $this->M_item->update($data);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Item Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Item Gagal diupdate', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function delete() {
		$id = $_POST['id'];
		$result = $this->M_item->delete($id);

		if ($result > 0) {
			echo show_succ_msg('Item Berhasil dihapus', '20px');
		} else {
			echo show_err_msg('Item Gagal dihapus', '20px');
		}
	}

	public function detail() {
		$data['userdata'] 	= $this->userdata;

		$id 				= trim($_POST['id']);
		// $data['kota'] = $this->M_kota->select_by_id($id);
		// $data['jumlahKota'] = $this->M_kota->total_rows();
		$data['dataStock'] = $this->M_stock->select_by_id_stock($id);

		echo show_my_modal('stock/modal_detail_stock', 'detail-stock', $data, 'lg');
	}

	public function export() {
		error_reporting(E_ALL);
    
		include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->M_stock->barang_keluar();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Tanggal Keluar");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Kode Item");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Category Item");
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Nama Item");
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Jumlah");
		$rowCount++;

		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->tanggal_keluar); 
		    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$rowCount, get_field($value->id_item,'tbl_item','kode_item'), PHPExcel_Cell_DataType::TYPE_STRING);
		    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, get_field($value->id_category,'category','nama_category')); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, get_field($value->id_item,'tbl_item','nama_item')); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->jumlah); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data Pegawai.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data Pegawai.xlsx', NULL);
	}


}